package com.kamatcho.example.texttospeach;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    TextToSpeech toSpeech;
    int result;
    String text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        EditText editText= (EditText)findViewById(R.id.Text);
         text = editText.getText().toString();
        toSpeech = new TextToSpeech(MainActivity.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS){
                    result = toSpeech.setLanguage(Locale.UK);

                }else {
                    Toast.makeText(MainActivity.this,"Ur Device Not Support That Language",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }
    public void dosomething(View view) {
        EditText editText= (EditText)findViewById(R.id.Text);
        text = editText.getText().toString();
        switch (view.getId()) {
            case R.id.Speak:
                if (result== TextToSpeech.LANG_NOT_SUPPORTED|| result == TextToSpeech.LANG_MISSING_DATA){
                    Toast.makeText(MainActivity.this,"Ur Device Not Support That Language",Toast.LENGTH_SHORT).show();
                }else {
                    toSpeech.speak(text,TextToSpeech.QUEUE_FLUSH,null);
                }
                break;
            case  R.id.Stop:
                if (toSpeech !=null){
                    toSpeech.stop();
                }
                break;

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (toSpeech!=null){
            toSpeech.stop();
            toSpeech.shutdown();
        }
    }
}